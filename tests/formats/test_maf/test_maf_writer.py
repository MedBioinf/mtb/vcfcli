import unittest, os
import adagenes


class TestMAFWriter(unittest.TestCase):

    def test_maf_writer(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../../test_files/somaticMutations.tcga.brca.maf"
        outfile = __location__ + "/../../test_files/somaticMutations.tcga.brca.maf.test.maf"
        genome_version = "hg38"

        bframe = adagenes.MAFReader().read_file(infile, genome_version=genome_version, max_length=100)

        adagenes.MAFWriter().write_to_file(outfile, bframe)

    def test_maf_writer_gz(self):
        pass


