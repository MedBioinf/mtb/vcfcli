import unittest
import adagenes


class TestToDataframe(unittest.TestCase):

    def test_to_df(self):
        data = { "chr7:140753336A>T":{ "revel":"0.98378","mvp":"0.986" },
                 "chr14:67885931T>G":{ "revel":"0.084","mvp":"." }
                 }
        bframe = adagenes.BiomarkerFrame()
        bframe.data = data

        df = adagenes.as_dataframe(bframe)

        columns = ["revel","mvp"]
        index = ["chr7:140753336A>T","chr14:67885931T>G"]

        self.assertListEqual(columns, list(df.columns),"Columns not correctly assigned")
        self.assertListEqual(index, list(df.index.values), "Index not correctly assigned")

