import unittest
import adagenes as ag


class TestCombinationTestCase(unittest.TestCase):

    def test_combination(self):
        data = {"chr7:140753336A>T":{}}
        bframe1 = ag.BiomarkerFrame(data, genome_version="hg38")

        data2 = {"chr1:354842insT":{}}
        bframe2 = ag.BiomarkerFrame(data2, genome_version="hg38")

        bframe = ag.merge_bframes(bframe1, bframe2, target_genome="hg38")
        print(bframe)
        self.assertEqual(2, len(list(bframe.data.keys())), "")
