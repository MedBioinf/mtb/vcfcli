import unittest, os
import adagenes as ag

class TestVCFToCSV(unittest.TestCase):

    def test_vcf_to_csv(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        #file1 = __location__ + "/../test_files/somaticMutations.ln50.txt"

        input_file= __location__ + "/../../tests/test_files/hg38_goflof_clinvar_v062021_seqcat.molfeat.protein.vcf"
        outfile = __location__ + "/../../tests/test_files/hg38_goflof_clinvar_v062021_seqcat.molfeat.protein.csv"
        iformat="vcf"
        oformat="maf"

        #data = adagenes.read_file(input_file)
        #print(data.data)
        #adagenes.write_file(outfile,data)
        ag.process_file(input_file, outfile, None)

        file = open(outfile)
        contents = file.read()
        #self.assertEqual(contents[0:50], "QID,CHROM,POS,POS_hg38,REF,ALT,type_desc,type,muta", "")
        file.close()

