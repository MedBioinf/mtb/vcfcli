import unittest, os
import adagenes
import adagenes.conf.read_config as conf_reader


class TestLiftoverClientClass(unittest.TestCase):

    def test_liftover_indel(self):
        data = { 'chr7:21784210insG': {} }
        bframe = adagenes.BiomarkerFrame(data=data, genome_version="hg19")
        print(bframe.data)

        bframe = adagenes.LiftoverClient(genome_version="hg19", target_genome="hg38").process_data(bframe)
        print(bframe.data)
        self.assertEqual(bframe.data['chr7:21744592insG']["variant_data"]["POS_hg38"], 21744592, "")

    def test_liftover_dict(self):
        data = {"chr7:140753336A>T": {"variant_data": {"CHROM": "7", "POS_hg38": "140753336"}}}
        genome_version = "hg38"

        import time
        start_time = time.time()

        client = adagenes.LiftoverClient(genome_version)
        print("Liftover (hg38tohg19)...")
        data = client.process_data(data, target_genome="hg19")
        bframe = adagenes.BiomarkerFrame(data=data, genome_version="hg19")

        stop_time = time.time() - start_time
        print(stop_time)

        self.assertEqual(int(bframe.data["chr7:140453136A>T"]["variant_data"]["POS_hg19"]), 140453136,
                         "Error retrieving hg19 position")
        self.assertEqual(int(bframe.data["chr7:140453136A>T"]["variant_data"]["POS_hg38"]), 140753336,
                         "Error retrieving hg38 position")
        self.assertEqual(int(bframe.data["chr7:140453136A>T"]["variant_data"]["POS"]), 140453136,
                         "Error retrieving default position")
        self.assertEqual(bframe.genome_version, "hg19", "Wrong genome version")
        print(client.extract_keys)
        print(client.key_labels)

    def test_liftover_hg38tohg19(self):
        data = { "chr7:140753336A>T": { "variant_data":{ "CHROM":"7", "POS_hg38":"140753336" } }}
        bframe = adagenes.BiomarkerFrame(data=data)
        genome_version="hg38"

        import time
        start_time = time.time()

        client = adagenes.LiftoverClient(genome_version)
        print("Liftover (hg38tohg19)...")
        bframe = client.process_data(bframe, target_genome="hg19")

        stop_time = time.time() - start_time
        print(stop_time)

        self.assertEqual(int(bframe.data["chr7:140453136A>T"]["variant_data"]["POS_hg19"]),140453136,
                         "Error retrieving hg19 position")
        self.assertEqual(int(bframe.data["chr7:140453136A>T"]["variant_data"]["POS_hg38"]), 140753336,
                         "Error retrieving hg38 position")
        self.assertEqual(int(bframe.data["chr7:140453136A>T"]["variant_data"]["POS"]), 140453136,
                         "Error retrieving default position")
        self.assertEqual(bframe.genome_version, "hg19", "Wrong genome version")

    def test_liftover_hg19tohg38(self):
        #data = { "chr7:140453136A>T": { "variant_data":{ "CHROM":"7", "POS_hg19":"140453136" } }}
        data = { "chr7:140453136A>T": {}, "TP53": {} }
        genome_version="hg19"

        import time
        start_time = time.time()
        client = adagenes.LiftoverClient(genome_version)
        bframe = adagenes.BiomarkerFrame(data=data, genome_version=genome_version)
        print(bframe.data)

        print("Liftover (hg19tohg38)...")
        bframe = client.process_data(bframe, target_genome="hg38")
        print(bframe.data)

        stop_time = time.time() - start_time
        print(stop_time)

        print(bframe.data.keys())

        self.assertListEqual(list(bframe.data.keys()),["chr7:140753336A>T","TP53"],"Keys do not match")
        self.assertEqual(int(bframe.data["chr7:140753336A>T"]["variant_data"]["POS_hg19"]),140453136,"Error retrieving hg19 position")
        self.assertEqual(int(bframe.data["chr7:140753336A>T"]["variant_data"]["POS_hg38"]), 140753336,
                         "Error retrieving hg38 position")
        self.assertEqual(int(bframe.data["chr7:140753336A>T"]["variant_data"]["POS"]), 140753336,
                         "Error retrieving default position")
        self.assertEqual(bframe.genome_version,"hg38","Wrong genome version")

    def test_liftover_hg19tohg38_batch(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        file1 = __location__ + "/../test_files/somaticMutations_brca_ln250.avf"
        bframe = adagenes.read_file(file1, genome_version="hg19")
        print(bframe.genome_version)
        print(len(bframe.data.keys()))
        bframe = adagenes.LiftoverClient(bframe.genome_version).process_data(bframe, target_genome="hg38")
        print("bframe data ",len(bframe.data.keys()))
        self.assertEqual(len(bframe.data.keys()),235,"Number of biomarkers does not match")
        self.assertEqual(str(bframe.data["chr10:8073950C>T"]["variant_data"]["POS_hg38"]),"8073950","liftover positions do not match")
        self.assertEqual(str(bframe.data["chr10:8073950C>T"]["variant_data"]["POS_hg19"]), "8115913",
                         "liftover positions do not match")

    def test_liftover_hg19tohg38_batch2(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        file1 = __location__ + "/../test_files/sample_brca.vcf"
        bframe = adagenes.read_file(file1, genome_version="hg19")
        print(bframe.genome_version)
        print(len(bframe.data.keys()))
        bframe = adagenes.LiftoverClient(bframe.genome_version).process_data(bframe, target_genome="hg38")
        print("bframe data2 ",len(bframe.data.keys()))

    def test_liftover_with_passed_lo_obj(self):
        data = { "chr7:140453136A>T": { "variant_data":{ "CHROM":"7", "POS_hg19":"140453136" } }}
        bframe = adagenes.BiomarkerFrame(data=data, genome_version="hg19")
        genome_version="hg19"
        #from pyliftover import LiftOver
        #lo=LiftOver(conf_reader.__LIFTOVER_DATA_DIR__ + "/hg19ToHg38.over.chain.gz")
        from liftover import ChainFile
        lo = ChainFile(conf_reader.__LIFTOVER_DATA_DIR__ + "/hg19ToHg38.over.chain.gz", one_based=True)

        import time
        start_time = time.time()
        client = adagenes.LiftoverClient(genome_version)
        print("Liftover (hg19tohg38) with preloaded liftover files...")
        bframe = client.process_data(bframe,lo_hg19=lo,lo_hg38=None, target_genome="hg38")

        stop_time = time.time() - start_time
        print("time: ",stop_time)
        #print(bframe.data)

    def test_liftover_t2t_to_hg38(self):
        genome_version="t2t"
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        file1 = __location__ + "/../test_files/chm13v0.9.snv.hifi.deepvariant_1.0.part.vcf"
        bframe = adagenes.read_file(file1, genome_version=genome_version)
        print(bframe.genome_version)
        print("Length after loading ", len(bframe.data.keys()))
        print(bframe.data.keys())
        print(bframe.data)
        #print(bframe.data["chr1:1592insT"])

        self.assertEqual(str(bframe.data["chr1:1593insT"]["variant_data"]["ALT"]), "CT",
                         "liftover positions do not match")

        bframe = adagenes.LiftoverClient(bframe.genome_version).process_data(bframe, target_genome="hg38")
        print("Length after liftover ",len(bframe.data.keys()))
        print("bframe data ",bframe.data.keys())
        print(bframe.data)
        outfile = __location__ + "/../test_files/chm13v0.9.snv.hifi.deepvariant_1.0.part.hg38.vcf"
        adagenes.write_file(outfile, bframe)
        self.assertEqual(len(bframe.data.keys()),59,"Number of biomarkers does not match")
        self.assertEqual(str(bframe.data["chr1:354843insT"]["variant_data"]["POS_t2t"]),"10236","liftover positions do not match")
        self.assertEqual(str(bframe.data["chr1:354843insT"]["mutation_type"]), "insertion",
                         "mutation type not detected")
        self.assertEqual(str(bframe.data["chr1:354843insT"]["variant_data"]["ALT"]), "CT",
                         "liftover positions do not match")

    def test_liftover_hg38_to_t2t(self):
        genome_version="hg38"
        data = {"chr7:140753336A>T": {"variant_data": {"CHROM": "7", "POS_hg38": "140753336A>T"}}}
        bframe = adagenes.BiomarkerFrame(genome_version=genome_version, data=data)
        bframe = adagenes.LiftoverClient(bframe.genome_version).process_data(bframe, target_genome="t2t")
        #print("bframe data ",bframe.data)
        #outfile = __location__ + "/../test_files/chm13v0.9.snv.hifi.deepvariant_1.0.part.hg38.vcf"
        #adagenes.write_file(outfile,bframe)
        print(bframe.data)
        self.assertEqual(len(bframe.data.keys()),1,"Number of biomarkers does not match")
        self.assertEqual(str(bframe.data["chr7:142067515A>T"]["variant_data"]["POS_t2t"]),"142067515","liftover positions do not match")
        self.assertEqual(str(bframe.data["chr7:142067515A>T"]["variant_data"]["POS_hg38"]), "140753336",
                         "liftover positions do not match")

    def test_liftover_hg19_to_hg38_braf(self):
        qid = "chr7:140453136A>T"
        variant_data= {qid:{}}
        variant_data = adagenes.LiftoverClient(genome_version="hg19").process_data(variant_data, target_genome="hg38")
        self.assertEqual(list(variant_data.keys()), ["chr7:140753336A>T"], "")

    def test_liftover_t2t_to_hg38_braf(self):
        qid = "chr7:142067515A>T"
        variant_data= {qid:{}}
        variant_data = adagenes.LiftoverClient(genome_version="t2t").process_data(variant_data, target_genome="hg38")
        self.assertEqual(list(variant_data.keys()), ["chr7:140753336A>T"], "")
