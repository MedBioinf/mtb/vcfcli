import unittest
import adagenes as ag


class TestPathogenicityHeatmapTestCase(unittest.TestCase):

    def test_pathogenicity_heatmap(self):
        revel = [1, 20, 30]
        am = [20, 1, 60]
        esm = [30, 60, 1]

        scores = [revel, am, esm]

        fig = ag.generate_protein_pathogenicity_plot(scores, x_title="Score", y_title="TP53",
                                                     x_labels=["a","b","c"], y_labels=["REVEL","AM","ESM1b"])
        fig.show()

