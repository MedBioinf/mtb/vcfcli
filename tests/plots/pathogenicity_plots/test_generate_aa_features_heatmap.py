import unittest, os
import adagenes as ag


class TestAAFeaturesHeatmapPlotGeneration(unittest.TestCase):

    def test_aafeatures_heatmap_plot_generation(self):
        #variants = {"TP53:V143A":{},"TP53:R175H":{},"TP53:K132Q":{},"TP53:E285K":{},"TP53:R280K":{},"TP53:R273H":{}}
        #score_labels_raw = ["REVEL_rankscore", "CADD_raw_rankscore", "AlphaMissense_rankscore", "ESM1b_rankscore",
        #                    "EVE_rankscore"]
        #score_data = ag.generate_aa_features_plot_data(variants)

        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../../test_files/tp53.interpreter.avf"
        variant_data = ag.read_file(infile).data
        #print(variant_data)
        variant_labels = list(variant_data.keys())

        fig = ag.generate_aa_features_heatmap(variant_data, variant_labels=variant_labels, width=600)
        fig.show()

    def test_aafeatures_heatmap_plot_generation_refalt(self):
        #variants = {"TP53:V143A":{},"TP53:R175H":{},"TP53:K132Q":{},"TP53:E285K":{},"TP53:R280K":{},"TP53:R273H":{}}
        #score_labels_raw = ["REVEL_rankscore", "CADD_raw_rankscore", "AlphaMissense_rankscore", "ESM1b_rankscore",
        #                    "EVE_rankscore"]
        #score_data = ag.generate_aa_features_plot_data(variants)

        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        infile = __location__ + "/../../test_files/tp53.interpreter.avf"
        variant_data = ag.read_file(infile).data
        variant_data = {list(variant_data.keys())[3]: variant_data[list(variant_data.keys())[3]]}
        #print(variant_data)
        variant_labels = list(variant_data.keys())

        fig = ag.generate_aa_features_heatmap_refalt(variant_data, variant_labels=variant_labels)
        #fig.show()




