

import unittest, os
import adagenes


class TestDefinedVariantFilter(unittest.TestCase):

    def test_defined_variant_filter(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        genome_version = 'hg19'

        infile = __location__ + "/../test_files/TumorVariantDownload_r20.uta.avf"
        bframe = adagenes.read_file(infile, genome_version=genome_version)

        self.assertEqual(len(list(bframe.data.keys())), 2107, "")
        print(bframe.data.keys())
        print(len(bframe.data.keys()))
        vars = ['chr17:7673814C>A', 'chr17:7674231G>A', 'chr17:7675137C>T', 'chr17:7675143C>T', 'chr17:7674950A>T', 'chr17:7675126G>A']
        bframe.data = adagenes.DefinedVariantFilter().process_data(bframe.data,vars)
        print(len(bframe.data.keys()))

        self.assertEqual(len(list(bframe.data.keys())), 6, "")


