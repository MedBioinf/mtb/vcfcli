
import unittest, os
import adagenes


class TestSectionFilter(unittest.TestCase):

    def test_section_filter(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        genome_version = 'hg19'

        infile = __location__ + "/../test_files/TumorVariantDownload_r20.uta.avf"
        bframe = adagenes.read_file(infile, genome_version=genome_version)
        print(len(bframe.data.keys()))
        bframe.data = adagenes.SectionFilter().process_data(bframe.data,"UTA_Adapter")
        print(len(bframe.data.keys()))
        print(bframe.data)


