

import unittest, os
import adagenes as ag


class TestPositionalSort(unittest.TestCase):

    def test_positional_sort(self):
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))
        genome_version = 'hg19'

        infile = __location__ + "/../test_files/TumorVariantDownload_r20.uta.avf"
        bframe = ag.read_file(infile, genome_version=genome_version)

        print(bframe.data.keys())
        print(len(bframe.data.keys()))
        bframe = ag.positional_sort(bframe)
        print(len(bframe.data.keys()))

        print("sorted: ",bframe.sorted_variants)
        #self.assertEqual(len(list(bframe.data.keys())), 6, "")


