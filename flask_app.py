import datetime
import os, copy, io
from flask_cors import CORS
from flask import jsonify, request, send_file
import adagenes.conf.read_config as conf_reader
import adagenes.app as agp
from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint
import adagenes.app as agp
import adagenes.tools.log_files
import adagenes.app.transform_model
import time
import math

DEBUG = True
SERVICE="adagenes"
VERSION="v1"

app = Flask(__name__)
app.config.from_object(__name__)

CORS(app, resources={r'/*': {'origins': '*'}})
SWAGGER_URL = '/adagenes/v1/docs'
API_URL = '/static/config.json'

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "AdaGenes"
    },
)

# definitions
SITE = {
    'version': '0.0.1'
}


@app.route(f'/{SERVICE}/{VERSION}/generate_new_id', methods=['POST'])
def generate_new_id():
    """
    Generate new ID and data

    :return:
    """
    new_id, new_data = None, None

    print("result for ", request.form.keys())

    if 'data_dir' in request.form:
        if request.form['data_dir'] == 'pub':
            data_dir = conf_reader.__DATA_DIR__
            id_file = conf_reader.__ID_FILE__
    else:
        data_dir = conf_reader.__DATA_DIR__
        id_file = conf_reader.__ID_FILE__
    #print("data dir ", data_dir)

    #res_data = onkopus_server.tools.data_mgt.generate_new_id_data(new_id, new_data, data_dir=data_dir, id_file=id_file)
    pid = agp.generate_new_id_data(None, data_dir=data_dir, id_file=id_file)

    #print("return new ID ", pid)
    return jsonify(pid)

@app.route(f'/{SERVICE}/{VERSION}/upload_variant_data', methods=['POST'])
def upload_variant_data():

    genome_version = 'hg38'
    if 'genome_version' in request.form:
        genome_version = request.form['genome_version']

    data_dir = 'tmp'
    if 'data_dir' in request.form:
        data_dir = request.form['data_dir']

    if 'id' in request.form:
        pid = request.form['id']
    else:
        pid = None

    if 'id_file' in request.form:
        id_file = request.form['id_file']
    else:
        id_file = conf_reader.__ID_FILE__

    if 'file_src' in request.form:
        file_src = request.form['file_src']
    else:
        file_src = None

    genome_version = ""
    if 'genome_version' in request.form:
        genome_version = request.form['genome_version']
    #print("Genome version ",genome_version)

    print("request keys: ", request.files.keys())
    if "file" in request.files:
        file = request.files['file']
    else:
        file = None

    if "file" in request.form:
        file = request.form["file"]

    pid = agp.add_biomarker_file_to_id(file, pid, data_dir, genome_version,
                                                                       file_src=file_src, id_file=id_file)

    return pid

@app.route(f'/{SERVICE}/{VERSION}/analyze_variant_request', methods=['GET'])
def analyze_variant_request_service():
    """
    Memory-based biomarker analysis intended to handle manual user variant queries
    Receives a comma-separated list of variants,
    performs a variant interpretation directly and returns the results as a dataframe

    :return:
    """
    query = request.args.get('q')
    gene_name_q = request.args.get('gene')
    genompos_q = request.args.get('genompos')
    tumor_type = request.args.get('tumor_type')
    sample = False
    genome_version_request = request.args.get('genome_version')

    # OncoKB header
    oncokb_key = ''
    if request.headers.get('oncokb-key') is not None:
        print("oncokb key passed: ",request.headers.get('oncokb-key'))
        oncokb_key = request.headers.get('oncokb-key')
    elif os.getenv("ONCOKB_KEY") is not None:
        oncokb_key = os.getenv("ONCOKB_KEY")


    # get cached response if request ID is given and found in the request cache
    request_id = request.args.get('request_id')

    annotated_data = agp.analyze_search(query, gene_name_q, genompos_q, genome_version=genome_version_request,
                                             oncokb_key=oncokb_key,lo_hg19=lo_hg19,lo_hg38=lo_hg38,lo_t2t=lo_t2t_to_hg38,
                                             tumor_type=tumor_type)

    return annotated_data


@app.route(f'/{SERVICE}/{VERSION}/analyze_uploaded_file', methods=['POST'])
def analyze_uploaded_file():
    """
    Analyzes file-based biomarker data analysis

    :param mode:0=perform analysis,1=perform analysis if no data is present

    :return:
    """
    data_dir = 'pub'
    genome_version = 'hg38'
    qid = None
    output_format = "vcf"
    data =None
    request_id = None
    output_genome_version = ''

    try:
        #if "data_dir" in request.form:
        #    data_dir = request.form['data_dir']
        #if "genome_version" in request.form:
        #    genome_version = request.form['genome_version']
        #if "id" in request.form:
        #    qid = request.form['id']
        #if "output_format" in request.form:
        #    output_format = request.form['output_format']
        #if "output_genome_version" in request.form:
        #    output_genome_version = request.form["output_genome_version"]

        data = request.json
        print("upload request ",data)
        output_genome_version = data.get("outputGenomeVersion", '')
        genome_version = data.get("genomeVersion", '')

    except:
        print("no input file given")
        infile = None
        outfile = None

    print("analysis request: id ", qid," output format ", output_format, " genome ", genome_version, ", data dir ",data_dir)
    data_dir_orig=copy.copy(data_dir)
    data_dir = conf_reader.__DATA_DIR__ #onkopus_server.processing.load_data.get_data_dir(data_dir)

    if data_dir_orig == "pub":
        data_dir_orig="tmp"

    column_defs, table_data, max_rows, filter_model = agp.analyze_uploaded_file(qid, data_dir=data_dir,
                                                        genome_version=genome_version,
                                                        output_genome_version = output_genome_version,
                                                        output_format=output_format)
    #annotated_data = agp.analyze_uploaded_file(infile=infile, outfile=outfile, mode=mode, module=module,
    #                                            qid=qid, data_dir=data_dir, data_dir_orig=data_dir_orig,
    #                                            genome_version=genome_version,
    #                                            oncokb_key=oncokb_key,
    #                                            output_format=output_format,lo_hg19=lo_hg19, lo_hg38=lo_hg38,
    #                                            data=data, request_id=request_id,
    #                                            request_cache=request_cache, tumor_type=tumor_type)

    gen_id = request_id
    print("return id ",gen_id)
    annotated_data = {"header":{"request_id": gen_id}, "table_data": table_data, "col_defs": column_defs}

    return annotated_data

dataset = [
    {
        "id": i + 1,
        "name": f"Name {i + 1}",
        "age": (i % 60) + 20,
        "country": ["USA", "Canada", "UK", "Australia"][i % 4],
        "profession": ["Engineer", "Doctor", "Teacher", "Artist"][i % 4],
    }
    for i in range(1000000)
]
dataset = [
    {
        "chrom": i + 1,
        "pos": f"Name {i + 1}",
        "ref": (i % 60) + 20,
        "alt": ["USA", "Canada", "UK", "Australia"][i % 4],
        "profession": ["Engineer", "Doctor", "Teacher", "Artist"][i % 4],
    }
    for i in range(1000000)
]


@app.route(f'/{SERVICE}/{VERSION}/getTableData', methods=["POST"])
def get_data():
    # Parse the request payload
    body = request.json
    start_row = body.get("startRow", 0)
    end_row = body.get("endRow", 100)
    sort_model = body.get("sortModel", [])
    filter_model = body.get("filterModel", {})
    annotations = body.get("annotations", {})
    output_genome_version = body.get("outputGenomeVersion", '')
    genome_version = body.get("genomeVersion", '')
    transform_model = body.get("transformations")
    transform_target_format = body.get("transformTargetFormat")
    qid = body.get("qid", "")

    print("request body ",body)

    output_format = "vcf"
    output_format = ""
    #if "transformTargetFormat" in request.keys():
    #    output_format = request.get("transformTargetFormat")
    output_format, transform_model = adagenes.app.transform_model.get_transform_model(qid, transform_target_format, transform_model, genome_version)

    data_dir = 'pub'
    #genome_version = 'hg38'
    #qid = None

    data = None
    request_id = None

    #print("output genome version ",output_genome_version)

    # get data
    try:
        #if "data_dir" in request.form:
        #    data_dir = request.form['data_dir']
        #if "genome_version" in request.form:
        #    genome_version = request.form['genome_version']
        #if "id" in request.form:
        #    qid = request.form['id']
        #if "output_format" in request.form:
        #    output_format = request.form['output_format']

        #data = request.json
        pass

    except:
        print("no input file given")
        infile = None
        outfile = None

    print("(get data) analysis request: id ", qid," output format ", output_format, " genome ", genome_version,
          "output genome ", output_genome_version, ", data dir ",data_dir)
    data_dir_orig=copy.copy(data_dir)
    data_dir = conf_reader.__DATA_DIR__ #onkopus_server.processing.load_data.get_data_dir(data_dir)

    if data_dir_orig == "pub":
        data_dir_orig="tmp"
    request_id = request.args.get('request_id')

    print(qid, ": ", start_row," - ",end_row)
    print("filter: ",filter_model)
    print("sort: ",sort_model)
    print("annotations: ", annotations)
    print("transform model: ",transform_model)

    if (qid =="") or (qid is None):
        print("return null")
        return {}

    # Recognize if filters or sort options are activated
    if (filter_active(filter_model) is False) and (sort_active(sort_model) is False):
        column_defs, table_data, max_rows, filter_model, output_genome_version = agp.analyze_uploaded_file(qid, start_row=start_row, end_row=end_row,
                                                                      data_dir=data_dir,
                                                                      genome_version=genome_version,
                                                                      output_genome_version=output_genome_version,
                                                                      output_format=output_format,
                                                                      annotate=annotations,
                                                                      sort_model = sort_model,
                                                                      filter_model = filter_model,
                                                                      transform_output_format=output_format,
                                                                      transform_model=transform_model
                                                                      )
        data = table_data
    else:
        column_defs, table_data, max_rows, filter_model, output_genome_version = agp.analyze_uploaded_file(qid,
                                                                      data_dir=data_dir,
                                                                      genome_version=genome_version,
                                                                      output_genome_version=output_genome_version,
                                                                      output_format=output_format,
                                                                      annotate=annotations,
                                                                      sort_model = sort_model,
                                                                      filter_model = filter_model,
                                                                      transform_output_format=output_format,
                                                                      transform_model=transform_model
                                                                      )
        data = table_data

        dataset = table_data
        end_row = body.get("endRow", len(dataset))
        #annotated_data = agp.analyze_uploaded_file(infile=infile, outfile=outfile, mode=mode, module=module,
        #                                            qid=qid, data_dir=data_dir, data_dir_orig=data_dir_orig,
        #                                            genome_version=genome_version,
        #                                            oncokb_key=oncokb_key,
        #                                            output_format=output_format,lo_hg19=lo_hg19, lo_hg38=lo_hg38,
        #                                            data=data, request_id=request_id,
        #                                            request_cache=request_cache, tumor_type=tumor_type)


    # Apply annotations
    #column_defs, data = annotate(data)

    # Apply pagination
    #print("col data ",column_defs)
    #print("dta", data)
    #paginated_data = data[start_row:end_row]

    print("f ",filter_model)
    print("max rows ",max_rows)
    print("genome ",output_genome_version)

    # Prepare response
    response = {
        "rows": data,
        "totalCount": max_rows,  # Total rows after filtering
        "columnDefs": column_defs,
        "filterModel": filter_model,
        "outputGenomeVersion": output_genome_version
    }
    response['totalCount'] = max_rows
    #print("return ",response)
    return jsonify(response)

    #gen_id = request_id
    #print("return id ",gen_id)
    ##annotated_data = {"header":{"request_id": gen_id}, "table_data": table_data, "col_defs": column_defs}
    #annotated_data = {"header": {"request_id": gen_id}, "response": jsonify(response), "col_defs": column_defs}
    #return annotated_data

@app.route(f'/{SERVICE}/{VERSION}/getTableColumns', methods=["POST"])
def get_columns():
    qid = request.form["qid"]

def filter_active(filter_model):
    filter_active = False
    for column, filter_data in filter_model.items():
        filter_active = True
        print("Filter active")
    return filter_active

def sort_active(sort_model):
    sort_active = False
    if sort_model:
        for sort in reversed(sort_model):  # AG Grid supports multi-column sorting
            if sort["sort"] is not None:
                sort_active = True
                print("sort active")
    return sort_active


@app.route(f'/{SERVICE}/{VERSION}/getFileDownload', methods=["POST"])
def get_file_download():
    body = request.json
    # print("request body ",body)
    #start_row = body.get("startRow", 0)
    #end_row = body.get("endRow", 100)
    #sort_model = body.get("sortModel", [])
    #filter_model = body.get("filterModel", {})

    #data_dir = 'pub'
    #genome_version = 'hg38'
    #qid = None
    #output_format = "vcf"
    #data = None
    #request_id = None

    datetime_str = str(datetime.datetime.now())
    datetime_str = datetime_str.replace(" ","")
    datetime_str = datetime_str.replace(":", "-")
    datetime_str = datetime_str.replace("/", "-")

    qid = body.get("qid", "")
    filetype = body.get("type", "csv")
    data_dir = conf_reader.__DATA_DIR__

    if filetype == "log":
        infile = data_dir + "/" + qid + "/log.txt"
        with open(infile, 'rb') as infile:
            file_content = infile.read()

        modified_content = adagenes.tools.log_files.modify_content(file_content)
        in_memory_file = io.BytesIO(modified_content)
        infile = in_memory_file

    elif filetype == "vcf":
        #bframe = agp.load_file(qid, data_dir)
        #column_defs, table_data = agp.generate_table_data_from_bframe(bframe, output_format=output_format)
        data_dir = data_dir + "/" + qid
        infile = adagenes.app.io.find_newest_file(data_dir)
    elif filetype == "csv":
        data_dir = data_dir + "/" + qid
        infile = agp.download_newest_file_as_csv(data_dir)
    print("Send file ",infile)

    return send_file(infile, as_attachment=True, download_name=qid + '_log_' + datetime_str + '.txt')


if __name__ == '__main__':
    app.register_blueprint(swaggerui_blueprint)
    app.run(host='0.0.0.0', debug=True, port=conf_reader.__ADAGENES_PORT__)
