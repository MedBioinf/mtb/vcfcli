import adagenes as av


def dataframe_to_bframe(df, file_type=None):
    """
    Converts a Pandas dataframe in a biomarker frame

    :param df:
    :param file_type:
    :return:
    """
    bframe = av.BiomarkerFrame()

    return bframe
