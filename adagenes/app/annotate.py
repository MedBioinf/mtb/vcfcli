import datetime
import onkopus as op
import adagenes as ag
from adagenes.app.io import find_newest_file, load_log_actions, append_to_file, split_filename
import adagenes.conf

def get_magic_obj(key, genome_version, transform_model = None):
    if key == "clinvar":
        return op.ClinVarClient(genome_version=genome_version)
    elif key == 'protein':
        return op.UTAAdapterClient(genome_version=genome_version)
    elif key == 'protein-to-gene':
        return op.CCSGeneToGenomicClient(genome_version=genome_version)
    elif key == 'transcripts':
        return op.GENCODEGenomicClient(genome_version=genome_version)
    elif key == 'dbsnp':
        return op.DBSNPClient(genome_version=genome_version)
    elif key == 'patho':
        return op.DBNSFPClient(genome_version=genome_version)
    elif key == 'molecular':
        return op.MolecularFeaturesClient(genome_version=genome_version)
    elif key == 'proteinfeatures':
        return op.ProteinFeatureClient(genome_version=genome_version)
    elif key == 'geneexpression':
        return op.GeneExpressionClient()
    elif key == 'proteinseq':
        return op.UTAAdapterProteinSequenceClient(genome_version=genome_version)
    elif key == 'functionalregions':
        return op.GENCODEGenomicClient(genome_version=genome_version)
    elif key == 'drug-gene-interactions':
        return op.DGIdbClient(genome_version=genome_version)
    elif key == 'clinical-evidence':
        return op.ClinSigClient(genome_version=genome_version)
    elif key == 'filter_text':
        return ag.TextFilter()
    elif key == 'filter_number':
        return ag.NumberFilter()
    elif key == 'hgvs':
        return ag.HGVSClient(genome_version=genome_version)
    elif key == 'transform-vcf':
        return ag.VCFTransformator(transform_model)
    else:
        return None


def annotate_qid(qid: str, annotations: dict, genome_version=None, data_dir=None,
                 output_format='vcf',
                 mapping=None):
    """
    Annotate variant data with selected filters, and stores the annotated data in a new file

    :param qid:
    :param annotations: Dictionary containing annotations as keys and true if the annotation should be performed, e.g. { 'clinvar' true, 'protein': true }
    :param genome_version:
    :param data_dir:
    :param output_format:
    :return:
    """

    annotation_requirements = {
        "transcripts": ["protein"]
    }

    print("Annotations ",annotations)
    print(qid)
    print(data_dir)
    if data_dir is None:
        data_dir = adagenes.conf.read_config.__DATA_DIR__
    data_dir = data_dir + "/" + qid
    logfile = data_dir + '/log.txt'

    for key in annotations.keys():
        if annotations[key]:
            print("Annotate: ", key, " ", qid)


            infile = find_newest_file(data_dir)
            if qid == "sample-vcf":
                infile= data_dir + ag.conf_reader.sample_file
            if qid == "sample-protein":
                infile= data_dir + ag.conf_reader.sample_protein_file

            #infile = split_filename(infile)
            infile_name = split_filename(infile)

            previous_actions = load_log_actions(logfile)
            print("loaded logfile ",logfile)
            print("previous actions: ",previous_actions)

            annotation_key = 'Annotation:'+key
            contains_substring = any(annotation_key in entry for entry in previous_actions)
            if contains_substring is False:
                datetime_str = str(datetime.datetime.now())
                print("infile ",infile)
                output_format = output_format.lstrip(".")
                outfile = infile_name + ".ann."  + datetime_str + "." + output_format
                outfile = outfile.replace(" ", "_")
                outfile = outfile.replace(":", "-")

                magic_obj = get_magic_obj(key, genome_version)
                print("annotate with magic obj ",magic_obj, " mapping ",mapping)
                ag.process_file(infile, outfile, magic_obj, mapping=mapping)

                append_to_file(logfile, annotation_key + "(" + datetime_str + ")::" + outfile + '\n')

                print("File annotated: ",outfile)
            else:
                print("Annotation already found: ",annotation_key)


    # Generate new column definitions:
    #, cellClass: "rag-blue"

